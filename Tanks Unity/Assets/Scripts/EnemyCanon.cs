﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyCanon : MonoBehaviour
{
    public float count;
    public Bullet bullet;
    void Start()
    {

        count = 2.5f;
    }

    
    void Update()
    {
        if (count <= 0)//el enemigo disparara cada 2.5Seg
        {
            
            count = 2.5f;
            Instantiate(bullet, transform.position, transform.rotation);
        }
        else
        {
            count -= Time.deltaTime;
        }

    }
}
