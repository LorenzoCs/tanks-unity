﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class InteractionZone : MonoBehaviour
{
    
    public Text showText;
    public string text;


    private void OnTriggerStay2D(Collider2D other)
    {

        //Se evalua que el objeto que este colisionando sea el player, en caso de que lo sea se muestra un texto
        //en pantalla.
       
        if (other.gameObject.tag == "Player")
        {
           
            showText.text = text;

        }
    }
    private void OnTriggerExit2D(Collider2D other)
    {
        //Cuando el player deja de colisionar con el objeto, el texto se vuelve vacio.
        
        showText.text = "";
    }
}
