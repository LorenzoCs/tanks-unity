﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyTankCanon : MonoBehaviour
{
    public PlayerMovment player;
    Vector3 dir;
    float angle;
    void Start()
    {
        player = FindObjectOfType<PlayerMovment>();



    }

    // este script queda obsoleto por utilizar la rotacion en enemyTank 
    void Update()
    {
        if (player != null)
        {
            dir = player.transform.position - transform.position;// vector player : (10,2) Vector enemy : (5,1) = (5,1)
            angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;//calculas el angulo, y despues lo pasas a radianes
            transform.rotation = Quaternion.AngleAxis(angle - 90, Vector3.forward);
        
        }



    }
}
