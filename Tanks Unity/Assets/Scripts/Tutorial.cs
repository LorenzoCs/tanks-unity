﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class Tutorial : MonoBehaviour
{
    public GameObject[] blanks;
    public int count;
    public float time=3f;
    public Text showTexts;
    public string text;
    void Update()
    {
        blanks = GameObject.FindGameObjectsWithTag("Blancos"); //Constantemente busco la cantidad de blancos en escena
        count = blanks.Length; //saco la cuantos elementos tiene el array
        if (count == 0)//si los elementos son 0 , gano el tutorial
        {
            showTexts.text = text;
            if (time <= 0)
            {
                SceneManager.LoadScene(2);
            }
            else
            {
                time -= Time.deltaTime;
            }

        }
        else
        {
            showTexts.text = "";
        }
    }
}
