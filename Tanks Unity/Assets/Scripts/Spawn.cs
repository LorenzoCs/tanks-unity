﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawn : MonoBehaviour
{
    public float count;
    public EnemyTank[] enemy;
    int index;
    void Start()
    {
        count = 2f;
    }

  
    void Update()
    {
        if (count <= 0)// cuando el contador es menor o igual a 0, se instancia un enemigo random  en la posicion de los spawns, luego espera 15 seg, para volver a hacerlo
        {
            index = Random.Range(0, enemy.Length-1);
            Instantiate(enemy[index], transform.position, transform.rotation);
            count = 15f;
        }
        else
        {
            if (count <= 15)
            {
                count -= Time.deltaTime;
            }
           
        }
        
    }
}
