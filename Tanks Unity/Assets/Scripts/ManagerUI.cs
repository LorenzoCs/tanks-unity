﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class ManagerUI : MonoBehaviour
{
    public Image image;
    public PlayerMovment player;
    float lifeCalculator;
    public int tanksKilleds;
    public int scene;
    public Text text;
    void Start()
    {
        
        player = FindObjectOfType<PlayerMovment>();
        tanksKilleds = 0; // esta variable 
    }

   
    void Update()
    {
        lifeCalculator= player.life / player.maxLife; // se calcula la vida actual / la vida maxima para ingresarlo en la variable
        
       image.fillAmount = lifeCalculator;// la variable da un numero entre 0 y 1, por lo tanto se lo pasa al fillAmount de la imagen de vida en escena
       if (tanksKilleds >= 10)// si se llega a un total de 10 tanques destruidos se cambia a la siguiente escena
        {
            SceneManager.LoadScene(scene);  
        }
        text.text = tanksKilleds.ToString()+"/10 tanques destruidos";
       if (player == null)//si el jugador es destruido, se redirecciona a la escena 6(perder)
        {
            
            SceneManager.LoadScene(6);
        }
       
    }
}
