﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    PlayerMovment player;
    void Start()
    {
        
        
        player = FindObjectOfType<PlayerMovment>();
    }

    
    void Update()
    {
        //este script se usa para que la camara siga al player
        
       
        if (player != null)
        {
            transform.position = new Vector3(player.transform.position.x, player.transform.position.y, -30f);
        }
       

    }
}
