﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponPlayer : MonoBehaviour
{
    public Bullet bullet;
    public Rigidbody2D rb;
    public float charge;
    public bool powerUpActivate;
    public float count;
    void Start()
    {
        rb = GetComponentInParent<Rigidbody2D>();
        charge=0.3f;
        powerUpActivate = false;
    }

   
    void Update()
    {

        if (charge <= 0)// para disparar se tiene que cumplir la condicion de que este pequeño contador llegue a 0, para que no pueda disparar demasiado
        {
            
            if (Input.GetKeyDown(KeyCode.Mouse0))
            {
                
                Instantiate(bullet, transform.position,transform.rotation);
                if (!powerUpActivate) { 
                charge = 0.3f;
                }
                else
                {
                    if (count >= 0)
                    {
                        charge = 0f;
                        
                    }
                    else
                    {
                        charge = 0.3f;
                        powerUpActivate = false;
                    }
                    
                }
            }
        }
        else
        {
            if (charge >= 0)
            { 
                charge -= Time.deltaTime;
            }
        }
        count -= Time.deltaTime;


    }
}
