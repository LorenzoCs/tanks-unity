﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class Bullet : MonoBehaviour
{
    // Start is called before the first frame update
    public int speedBullet;
    public float timeLife;
    void Start()
    {
        speedBullet = 25;
        timeLife = 3f;
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(0f, speedBullet * Time.deltaTime, 0f); //se mueve hacia adelante a la velocidad speedBullet
        if (timeLife <= 0)//despues de un determinado tiempo se destruye
        {
            Destroy(gameObject);
        }
        else
        {
            timeLife -= Time.deltaTime;
        }
    }
    private void OnTriggerEnter2D(Collider2D collision) //si la bala choca con algo que no sea zone (blancos del tutorial) se destruye 
    {
        if (collision.gameObject.tag != "Zone"&& collision.gameObject.tag != "Obstacles")
        { 
        Destroy(gameObject);
        }
        
    }
   
}
