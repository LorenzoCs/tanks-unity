﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyTank : MonoBehaviour
{
    public float life;

    public GameObject[] posPatrol;
    public GameObject[] posPatrolSpecial;
    public int index;
    public PlayerMovment player;
    public SpriteRenderer[] sprite;
    float angle;
    public float count;
    public Vector3 dir;
    public ManagerUI manager;
    public int speed;
    public int tankId;
    public int probability;
    public int probabilityIndex;
    public GameObject[] powerUp;

    void Start()
    {
        
        posPatrol=GameObject.FindGameObjectsWithTag("Point");
        posPatrolSpecial = GameObject.FindGameObjectsWithTag("Point2");
        manager = FindObjectOfType<ManagerUI>();
        speed = 6;
        index = 0;
        count = 3f;
        player = GameObject.FindObjectOfType<PlayerMovment>();
        sprite=GetComponentsInChildren<SpriteRenderer>();
        
    }
    void Update()
    {     
        if (tankId == 0)
        {
            if (player != null)
            {

                dir = player.transform.position - transform.position; // dir es la distancia entre el player y el enemigo
                transform.position += dir.normalized * Time.deltaTime * speed; // el enemigo se movera a la posicion del player. La normalización de un vector le "quita" la magnitud, dejándole con solo una dirección ya que un vector esta compuesto por una direccion y una magnitud
                angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg; //calculas el angulo , y despues lo pasas a radianes 
                                                                   //atan2 , devuelve el arco tangente, de las coordenadas X e Y especificadas.
                                                                   //rad2deg convierte el numero en radianes a su equivalente en grados
                transform.rotation = Quaternion.AngleAxis(angle - 90, Vector3.forward); //establezco una rotacion de el angulo-90 y se dirige hacia adelante
            }
            if (life <= 0)// si la vida del tanque se acaba le sumo a la variable de manager 1 y luego destruyo el tanque
            {

                manager.tanksKilleds += 1;
                
                Destroy(gameObject);
            }

        }
        if (tankId == 1)
        {
           
            if (player != null)
        {
                
            dir = player.transform.position - transform.position; // dir es la distancia entre el player y el enemigo
            transform.position += dir.normalized * Time.deltaTime * speed; // el enemigo se movera a la posicion del player. La normalización de un vector le "quita" la magnitud, dejándole con solo una dirección ya que un vector esta compuesto por una direccion y una magnitud
            angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg; //calculas el angulo , y despues lo pasas a radianes 
                                                               //atan2 , devuelve el arco tangente, de las coordenadas X e Y especificadas.
                                                               //rad2deg convierte el numero en radianes a su equivalente en grados
            transform.rotation = Quaternion.AngleAxis(angle - 90, Vector3.forward); //establezco una rotacion de el angulo-90 y se dirige hacia adelante
        }
        if (life <= 0)// si la vida del tanque se acaba le sumo a la variable de manager 1 y luego destruyo el tanque
        {

            manager.tanksKilleds += 1;
                probability = Random.Range(1, 5);
                print(probability);

                if (probability == 1)
                {
                    probabilityIndex = Random.Range(0, 3);
                    Instantiate(powerUp[probabilityIndex], transform.position, transform.rotation);
                }
                Destroy(gameObject);
        }
        }
        if (tankId == 2)
        {
            if (player != null)
            {
                

                dir = posPatrol[index].transform.position - transform.position;// dir es la distancia entre el player y el enemigo
                transform.position += dir.normalized * Time.deltaTime * speed; // el enemigo se movera a la posicion del player. La normalización de un vector le "quita" la magnitud, dejándole con solo una dirección ya que un vector esta compuesto por una direccion y una magnitud
                angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg; //calculas el angulo , y despues lo pasas a radianes 
                                                                   //atan2 , devuelve el arco tangente, de las coordenadas X e Y especificadas.
                                                                   //rad2deg convierte el numero en radianes a su equivalente en grados
                transform.rotation = Quaternion.AngleAxis(angle - 90, Vector3.forward); //establezco una rotacion de el angulo-90 y se dirige hacia adelante
            }
            if (life <= 0)// si la vida del tanque se acaba le sumo a la variable de manager 1 y luego destruyo el tanque
            {

                manager.tanksKilleds += 1;
                probability = Random.Range(1, 5);


                if (probability == 1)
                {
                    probabilityIndex = Random.Range(0, 3);
                    Instantiate(powerUp[probabilityIndex], transform.position, transform.rotation);
                }
                Destroy(gameObject);
            }
        }
        if (tankId == 3)
        {
            if (count >= -3)
            {
                count -= Time.deltaTime;
            }
            else
            {
                count = 3f;
            }
        
            if (count >= 0)
            {
                speed = 16;
                sprite[0].color = new Color(1f, 1f, 1f, .2f);
                sprite[1].color = new Color(1f, 1f, 1f, .2f);
            }
            else
            {
                speed = 6;
                sprite[0].color = new Color(1f, 1f, 1f, 1f);
                sprite[1].color = new Color(1f, 1f, 1f, 1f);
            }

            if (player != null)
            {


                dir = posPatrolSpecial[index].transform.position - transform.position;// dir es la distancia entre el player y el enemigo
                transform.position += dir.normalized * Time.deltaTime * speed; // el enemigo se movera a la posicion del player. La normalización de un vector le "quita" la magnitud, dejándole con solo una dirección ya que un vector esta compuesto por una direccion y una magnitud
                angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg; //calculas el angulo , y despues lo pasas a radianes 
                                                                   //atan2 , devuelve el arco tangente, de las coordenadas X e Y especificadas.
                                                                   //rad2deg convierte el numero en radianes a su equivalente en grados
                transform.rotation = Quaternion.AngleAxis(angle - 90, Vector3.forward); //establezco una rotacion de el angulo-90 y se dirige hacia adelante

            }
            if (life <= 0)// si la vida del tanque se acaba le sumo a la variable de manager 1 y luego destruyo el tanque
            {

                manager.tanksKilleds += 1;
                probability = Random.Range(1, 5);
                                                    //al morir existe una probabiidad de 4/1 que genere un powerup
                if (probability == 1)//si la probabilidad es 1, elige uno de los powerups aleatoriamente
                {
                    probabilityIndex = Random.Range(0, 3);
                    Instantiate(powerUp[probabilityIndex], transform.position, transform.rotation);
                }
                Destroy(gameObject);
            }
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
       // dependiendo si el tanque enemigo es colisionado por un tipo de bala, le resta mas o menos vida 
        if (collision.gameObject.tag == "Bullet")
        {
           
            life -= 1;
            
            
        }
        if (collision.gameObject.tag == "Bullet2")
        {
            
            life -= 0.05f;

            
        }
        if (collision.gameObject.tag == "Bullet3")
        {

            life -= 0.1f;


        }
        if (tankId == 2)
        {
           if(collision.gameObject.tag == "Point")
            {
                index++;
                    if (index == posPatrol.Length)
                {
                    index = 0;
                }
            }

        }
        if (tankId == 3)
        {
            if (collision.gameObject.tag == "Point2")
            {
                
                index++;
                if (index == posPatrolSpecial.Length)
                {
                    index = 0;
                }
            }

        }
        if (collision.gameObject.tag == "Obstacles")
        {


            speed = speed / 2;
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Obstacles")
        {
            speed = speed * 2;
        }
    }
}
