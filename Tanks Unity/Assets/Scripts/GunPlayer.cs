﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunPlayer : MonoBehaviour
{
    public Bullet bullet;
    public Rigidbody2D rb;
    public float charge;
    public bool powerUpActivate;
    public Bullet bulletSpecial;
    public float count;
    void Start()
    {
        powerUpActivate = false;
        rb = GetComponentInParent<Rigidbody2D>();
        charge = 0.3f;
        count = 0f;
    }


    void Update()
    {

        if (charge <= 0)//bala de ametralladora se disparara cada 0.05 seg , salvo que este activado el powerUp que dispara mas lento pero con una bala mas fuerte
        {

            if (Input.GetKey(KeyCode.Mouse1) || Input.GetKey(KeyCode.LeftShift))
            {

                if (!powerUpActivate)
                {
                    Instantiate(bullet, transform.position, transform.rotation);
                    charge = 0.05f;
                }
                else
                {
                    if (count >= 0)
                    {
                        Instantiate(bulletSpecial, transform.position, transform.rotation);
                        charge = 0.10f;

                    }
                    else
                    {
                        charge = 0.10f;
                        powerUpActivate = false;
                    }

                }
            }
            
        }
        else
        {
            if (charge >= 0)
            {
                charge -= Time.deltaTime;
            }

        }
        count -= Time.deltaTime;
    }
}
