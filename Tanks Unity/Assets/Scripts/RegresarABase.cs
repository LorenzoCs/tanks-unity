﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RegresarABase : MonoBehaviour
{
    public PlayerMovment player;
    public Vector3 positionInitial;  
    public Quaternion rotationInitial;
    private void Start()
    {

        player = FindObjectOfType<PlayerMovment>();
        positionInitial = player.transform.position;        //guardo la posicion inicial del player
        rotationInitial = player.transform.rotation;        //guardo la rotacion inicial del player
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")     //  Si la pared es tocada por el player, manda al player modificando su posicion y rotacion a la inicial
        {
            
            player.transform.position = positionInitial;

            player.transform.rotation = rotationInitial;
           
        }
    }
}
