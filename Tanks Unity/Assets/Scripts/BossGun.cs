﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossGun : MonoBehaviour
{

    public float count;
    
    public GameObject bullet;
    void Start()
    {
        count = 0.5f;
    }

    // Update is called once per frame
    void Update()
    {

        if (count <= 0)// cuando el contador es menor o igual a 0, se instancia un enemigo random  en la posicion de los spawns, luego espera 15 seg, para volver a hacerlo
        {
            
            Instantiate(bullet, transform.position, transform.rotation);
            count = 0.5f;
        }
        else
        {
            if (count <= 0.5)
            {
                count -= Time.deltaTime;
            }

        }

    }
}
