﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUps : MonoBehaviour
{
    public int idPower;
    public PlayerMovment player;
    public WeaponPlayer weaponPlayer;
    public GunPlayer [] gunPlayer;
    void Start()
    {
        player = FindObjectOfType<PlayerMovment>();
        weaponPlayer = FindObjectOfType<WeaponPlayer>();
        gunPlayer = FindObjectsOfType<GunPlayer>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag=="Player")
        {
            if (idPower == 1)//1aceite
            {
                player.count = 5f;
                player.powerUpActivate = true;

                Destroy(gameObject);
            }
            if (idPower == 2)//bullet
            {
                weaponPlayer.count = 5f;
                gunPlayer[0].count = 5f;
                gunPlayer[0].powerUpActivate = true;
                gunPlayer[1].count = 5f;
                gunPlayer[1].powerUpActivate = true;
                weaponPlayer.powerUpActivate = true;
                Destroy(gameObject);
            }
            if (idPower == 3)//vida
            {
                player.life = player.maxLife;
                Destroy(gameObject);
            }
        }
    }
}
