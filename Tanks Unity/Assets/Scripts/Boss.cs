﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class Boss : MonoBehaviour
{
    public EnemyTank enemy;
    void Start()
    {
        enemy = FindObjectOfType<EnemyTank>();
    }

    //este script se usa solo para saber si el boss esta en escena, si no lo esta significa que puede pasar de escena , y ya ganaria
    void Update()
    {
        if (enemy == null)
        {
            SceneManager.LoadScene(7);
        }
    }
}
