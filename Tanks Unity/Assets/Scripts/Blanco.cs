﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Blanco : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Bullet" || collision.gameObject.tag=="Bullet2")  // Si un blanco es tocado por una bala, es destruido
        {
            Destroy(gameObject);
        }
    }
}
