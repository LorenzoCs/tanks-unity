﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovment : MonoBehaviour
{


    public float life;
    public int speedInitial;
    public int speed;

    public Rigidbody2D rb;
    public float maxLife=5;
    public bool powerUpActivate;
    public float count;
    public bool speedController;
    void Start()
    {
        speedController = true;
        life = maxLife;
        speedInitial = 1000;
        
        rb = GetComponent<Rigidbody2D>();
    }

   
    void Update()



    {
       if (!powerUpActivate)
        {
            if (speedController)
            {
                speed = speedInitial;
            }
            else
            {
                speed =speedInitial/2;
            }
            
            

        }
        else
        {
            if (speedController)
            {
                if (count >= 0)
                {
                    speed = 1400;

                    count -= Time.deltaTime;
                }
                else
                {
                    speed = speedInitial;
                    powerUpActivate = false;

                }
            }
            else
            {
                if (count >= 0)
                {
                    speed = 700;

                    count -= Time.deltaTime;
                }
                else
                {
                    speed = speedInitial;
                    powerUpActivate = false;

                }
            }
        }
        if (Input.GetKey(KeyCode.W))
        {
            rb.AddForce(transform.up * speed*Time.deltaTime);
        }
        if (Input.GetKey(KeyCode.S))
        {
            rb.AddForce(-transform.up * speed * Time.deltaTime);
        }
        if (Input.GetKey(KeyCode.A))
        {
            rb.rotation += 100.5f * Time.deltaTime;
        }
        if (Input.GetKey(KeyCode.D))
        {
            rb.rotation += -100.5f * Time.deltaTime;
        }

        


    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        // si la bala colisiona con el player le resta 1 de vida , si la bala es del boss 2.5
        //si colisiona con obstacle su velocidad se ve reducida
        if (collision.gameObject.tag == "Bullet"){ 
         life -= 1;
            if (life <= 0)
            {
                Destroy(gameObject);
            }
        }
        if (collision.gameObject.tag == "Bullet2")
        {
            life -= 0.05f;
            if (life <= 0)
            {
                Destroy(gameObject);
            }
        }
        if (collision.gameObject.tag == "BulletBoss")
        {
            life -= 2.5f;
            if (life <= 0)
            {
                Destroy(gameObject);
            }
        }
        if (collision.gameObject.tag == "Obstacles")
        {

            speedController = false;

        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Obstacles")
        {
            speedController = true;
        }
    }
    
    
        
    




}
