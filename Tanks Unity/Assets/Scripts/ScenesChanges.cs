﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class ScenesChanges : MonoBehaviour
{
    public int scene;
    public void SceneChange()
    {
        SceneManager.LoadScene(scene);
    }
    public void Quit()
    {
        Application.Quit();

    }
}
